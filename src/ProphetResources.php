<?php


    namespace Prophet\ProphetResources;

    use Illuminate\Support\Facades\DB;
    use Auth;

    class ProphetResources
    {

        public function html($site,$organisationId=null){
            return view('ProphetResources::table',['resources' =>$this->resources($site,$organisationId)])->render();
        }

        public function resources($site,$organisationId=null){

            $query = DB::table('tblResources')
                ->select(
                    'tblResources.id AS id',
                    'tblResources.name AS name',
                    'tblResources.position AS position',
                    'tblResourcesSections.name AS section',
                    'download',
                    'view',
                    'button',
                    'restricted_to_organisation_json'
                )
                ->join('tblResourcesSections', 'tblResourcesSections.id', '=', 'tblResources.section_id')
                ->orderBy('tblResourcesSections.position')
                ->orderBy('position')
                ->whereJsonContains('prophet_sites_json',$site)
                ->get();

            $resources = array();
            foreach ($query as $r) {
                $skip = false;

                $restrictedToOrganisationsList = json_decode($r->restricted_to_organisation_json);
                if(is_array($restrictedToOrganisationsList) && !$organisationId){
                    $skip = true;
                }

                if(is_array($restrictedToOrganisationsList) && $organisationId){
                    if($restrictedToOrganisationsList[0]!=""){
                        $skip = !in_array($organisationId,$restrictedToOrganisationsList);
                    }
                }

                if(!$skip){
                    $resources[$r->section][] = [
                        'name'     => $r->name,
                        'position' => $r->position,
                        'download' => $r->download,
                        'view'     => $r->view,
                        'button'     => $r->button,
                    ];
                }
            }

            return $resources;

        }

        public function videos(){
            return view('ProphetResources::videos')->render();
        }
    }
