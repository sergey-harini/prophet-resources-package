@foreach($resources as $section => $downloads)
    @php ($line = 0)
    <div class="row">
        <div class="col-12">
            <h4> {{$section}}:</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <table class="table">
                @foreach($downloads as $r)
                    @php ($line++)
                        <tr>
                            <td style="width: 30px;">{{$line}}</td>
                            <td class="text-left">{{$r['name']}}</td>
                            <td style="width: 150px;" >
                                @if(!$r['view'])
                                    <a href="/resource/view/{{$r['download']}}"  class="btn btn-prophet-soft btn-sm btn-block">
                                        View <i class="far fa-file-pdf"></i>
                                    </a>
                                @else
                                    {!! $r['view'] !!}
                                @endif
                            </td>
                            <td style="width: 150px;" >
                                @if($r['download'])
                                    <a href="/resource/download/{{$r['download']}}"  class="btn btn-prophet-soft btn-sm btn-block">
                                        Download <i class="far fa-file-pdf"></i>
                                    </a>
                                @endif
                            </td>
                        </tr>
                @endforeach
            </table>
        </div>
    </div>
 @endforeach

