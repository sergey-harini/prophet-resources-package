<?php

    Route::group(['middleware' => ['web','auth']], function () {
        Route::get('/resource/download/{file}', 'Prophet\ProphetResources\DownloadController@download');
        Route::get('/resource/view/{file}', 'Prophet\ProphetResources\DownloadController@view');
    });
