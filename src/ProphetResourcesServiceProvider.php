<?php

namespace Prophet\ProphetResources;

use Illuminate\Support\ServiceProvider;
use App;

class ProphetResourcesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        $this->mergeConfigFrom( __DIR__ . '/config.php', 'prophet');

        $this->loadViewsFrom(__DIR__.'/views', 'ProphetResources');
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        $this->app->make('Prophet\ProphetResources\DownloadController');

        App::bind('ProphetResources', function()
        {
            return new ProphetResources();
        });

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
