<?php

    return [
        'resources_path' => env('PROPHET_RESOURCES_PATH'),
        'resources_url' => env('PROPHET_RESOURCES_URL'),
    ];