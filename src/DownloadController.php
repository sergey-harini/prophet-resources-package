<?php

namespace Prophet\ProphetResources;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;

class DownloadController extends Controller
{
    public function view($file){
        return $this->getPdf($file,'inline');
    }

    public function download($file){
        return $this->getPdf($file,'attachment');
    }

    private function getPdf($file,$action){

        $resource = DB::table('tblResources')
            ->select('s3_key','download')
            ->where('download',$file)
            ->first();

        if(!$resource){
            abort(404);
        }

        $mimetype = \GuzzleHttp\Psr7\mimetype_from_filename( $resource->download);

        $headers = [
            'Content-Type'        => $mimetype,
            'Content-Disposition' => $action.'; filename="'. $resource->download .'"',
        ];

        try {
            return \Response::make(Storage::disk('s3')->get($resource->s3_key), 200, $headers);
        }
        catch(\Exception $e){
            abort(404);
        }

    }
}
