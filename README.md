# PROPHET Resources
Enables us to use the same Rsources on all the PROPHET websites.
## Installation

Copy packages/prophetresourses

Add to .env
```bash
PROPHET_RESOURCES_URL=http://prophet-resources.test/
PROPHET_RESOURCES_PATH=/var/www_prophet-resources/
```

Add to composer.json
```bash
"autoload": {
        "psr-4": {
            "Tests\\": "tests/",
            "Prophet\\ProphetResources\\": "packages/prophet/prophetresources/src"
        }
    },
```

Add to config/app.php

Provider:
```bash
Prophet\ProphetResources\ProphetResourcesServiceProvider::class,
```
Ailas:
```bash
'ProphetResources' => Prophet\ProphetResources\Facades\ProphetResources::class,
 ```
 
Add to app.js
```bash
require('../../packages/prophet/prophetresources/src/prophet-resources.js')
```

## Usage

Include the following in the resources view file
  
```bash  
{!! ProphetResources::get('main') !!}
{!! ProphetResources::get('samples') !!}
{!! ProphetResources::videos() !!}
```

## Credits

- [Russ Chandler](https://russchandler.co,)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
 
